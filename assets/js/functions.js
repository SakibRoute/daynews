//News Ticker Active
$('#js-news').ticker();


// Sticky Sidebar active
    $('.sticky-column').theiaStickySidebar();

//feature-post-section style5 slider swiper
    var swiper = new Swiper('.feature-post-slide-container', {
      slidesPerView: 5,
      spaceBetween: 5,
      slidesPerGroup: 1,
      loop: true,
      loopFillGroupWithBlank: true,
      breakpoints: {
        1024: {
          slidesPerView: 4,
          spaceBetween: 5,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 5,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 5,
        },
        320: {
          slidesPerView: 1,
          spaceBetween: 5,
        }
      }
    });



var swiper = new Swiper('.client-container', {
      slidesPerView: 5,
      spaceBetween: 30,
      slidesPerGroup: 1,
      loop: true,
      loopFillGroupWithBlank: true,
    });



 //mobile menu display
     $('.mobile-toggle').on('click', function() {
          $('body').addClass('open-mobile-menu')
      });
 $('.close').on('click', function() {
          $('body').removeClass('open-mobile-menu')
      });
//mobile drodown menu display
 $('.mobile-menu-area ul>li>a,.mobile-menu-area ul.mobile-submenu>li>a').on('click', function() {
  var element = $(this).parent('li');
  if (element.hasClass('open')) {
    element.removeClass('open');
    element.find('li').removeClass('open');
    element.find('ul').slideUp(1000,"swing");
  }
  else {
    element.addClass('open');
    element.children('ul').slideDown(1000,"swing");
    element.siblings('li').children('ul').slideUp(1000,"swing");
    element.siblings('li').removeClass('open');
    element.siblings('li').find('li').removeClass('open');
    element.siblings('li').find('ul').slideUp(1000,"swing");
  }
}); 



//menu options
  var fixed_top = $(".header-fixed-part");

  $(window).on('scroll', function(){
    
    if( $(this).scrollTop() > 100 ){  
      fixed_top.addClass("animated fadeInDown menu-fixed");
    }
    else{
      fixed_top.removeClass("animated fadeInDown menu-fixed");
    }
  });